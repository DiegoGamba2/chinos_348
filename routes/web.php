<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//primera ruta
Route::get('hola',function(){
echo "Hola estoy en laravel";

});
Route::get('arreglo',function(){
	$estudiantes=["AD"=>"Andres","LA"=>"Laura","RO"=>"Rodrigo" ];
foreach ($estudiantes as $indice => $estudiante) {
	echo "$estudiante tiene indice $indice <hr />";
}
});
Route::get('paises',function(){
	
//crear un arreglo con información de países
	$paises = ["Colombia" => 
	["Capital" => "Bogotá",
	 "Moneda" => "Peso",
	  "poblacion" => 50.372]

	,"Ecuador" => 
	["Capital" => "Quito",
	"Moneda" => "Dolar",
	"poblacion"=> 17.517]

	, "Brazil" =>
	["Capital" => "Brazilia",
	"Moneda" => "Real",
	 "poblacion" => 212.216]

	,"Bolivia" =>
	["Capital" => "La paz",
	"Moneda" => "Boliviano",
	 "poblacion" => 11.633]];
/*recorrer la primera dimension
foreach ($paises as $pais =>$infopais){
	echo "<h2> $pais </h2>";
	echo "capital".$infopais["Capital"]."<br />";
	echo "moneda".$infopais["Moneda"]."<br />";
	echo "poblacion(en millones de habitantes)".$infopais["poblacion"]."<br />";
echo "<hr />";
}*/
// mostrar una vista para presentar los paises 
//En MVC yo puedo pasar datos a una vista
return view('paises')-> with("paises",$paises);	
});
